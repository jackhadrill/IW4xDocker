# IW4x in Docker

[![Build Status](https://drone.jacknet.io/api/badges/jackhadrill/IW4xDocker/status.svg)](https://drone.jacknet.io/jackhadrill/IW4xDocker)

This repository contains the necessary ingredients for running IW4x in Docker.

## Usage instructions

Use the following `docker-compose.yml` file in order to setup IW4x.

```yml
version: '3'
services:
  iw4x:
    image: git.jacknet.io/jackhadrill/iw4xdocker:latest
    volumes:
      - data: /game
    ports:
      - '28961:28961/tcp'
      - '28961:28961/udp'
volumes:
  data:
```

Follow the instructions [here](https://xlabs.dev/support_iw4x_server) in order to download the IW4x files and prepare them for use.

Once done, place these in the `data` volume such that `iw4x.exe` is mounted to `/game/iw4x.exe` within the container.

Configuration files should be placed in `userraw/server.cfg` within the `data` volume.