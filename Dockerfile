FROM ubuntu:22.04

WORKDIR /game
RUN dpkg --add-architecture i386 && apt-get update && apt-get -y install wine32 && apt-get clean

CMD [ "wine", "/game/iw4x.exe", "-dedicated", "-stdout", "+set", "net_port", "26961", "+exec", "server.cfg", "+party_enable", "0", "+sv_maxclients", "20", "+map_rotate" ]